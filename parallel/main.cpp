#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <time.h>
#include <pthread.h>
#include <unistd.h>


#define TRAIN "train_"
#define CSV ".csv"
#define WEIGHTS "weights.csv"
#define MAX 1
#define MIN 0
#define NTHREAD 4

using namespace std;

vector<vector<double>> readCSV(string addr){
    fstream csv;
    vector<vector<double>> out;
    vector<double> record;
    string line, word;

    csv.open(addr, ios::in);
    getline(csv, line); //flush header
    while(getline(csv, line)){
        record.clear();
        stringstream ss(line);
        while(getline(ss, word, ',')){
            record.push_back(stof(word.c_str()));
        }
        out.push_back(record);
    }
    return out;
}

vector<double> getLastCol(vector<vector<double>>& table){
    vector<double> result;
    int record_size = table[0].size();
    for(int i = 0; i < table.size(); i++){
        result.push_back(table[i][record_size - 1]);
    }
    return result;
}

vector<double> getColMaxMin(vector<vector<double>>& table, int type){
    vector<double> result;
    for(int i = 0; i < table[0].size(); i++) result.push_back(table[0][i]);
    for(int i = 0; i < table.size(); i++){
        for(int j = 0; j < table[i].size(); j++){
            switch (type){
            case MIN:
                if(result[j] > table[i][j])
                    result[j] = table[i][j];
                break;
            case MAX:
                if(result[j] < table[i][j])
                    result[j] = table[i][j]; 
                break;
            } 
        }
    }
    return result;
}

void normalize(vector<vector<double>>* table, vector<double>* colMax, vector<double>* colMin){
    for(int i = 0; i < (*table).size(); i++){
        for(int j = 0; j < (*table)[i].size() - 1; j++){
            if((*colMax)[j] != (*colMin)[j])
                (*table)[i][j] = ((*table)[i][j] - (*colMin)[j]) / ((*colMax)[j] - (*colMin)[j]);
            else if((*colMax)[j] != 0)
                (*table)[i][j] = 1;
        }
    }
}

double dotProduct(vector<double>& x, vector<double>& y){
    double result = 0;
    for(int i = 0;i < x.size() - 1; i++)
        result += x[i] * y[i];
    return result;
}

vector<vector<double>> getPrediction(vector<vector<double>>* weights, vector<double>* bias, vector<vector<double>>* train){
    vector<vector<double>> predictions;
    vector<double> prediction;
    for(int i = 0; i < (*train).size(); i++){
        prediction.clear();
        for(int j = 0; j < (*weights).size(); j++){
            prediction.push_back(dotProduct((*train)[i], (*weights)[j]) + (*bias)[j]);
        }
        predictions.push_back(prediction);
    }
    return predictions;
}

double getMaxIndex(vector<double>& pred){
    int maxIndex = 0;
    double _max = pred[0];
    for(int i = 0; i < pred.size(); i++){
        if(pred[i] > _max){
            _max = pred[i];
            maxIndex = i;
        }
    } 
    return maxIndex;
}

vector<double> getClass(vector<vector<double>>& preds){
    vector<double> classes;
    for(int i = 0; i < preds.size(); i++){
        classes.push_back(getMaxIndex(preds[i]));
    }
    return classes;
}

double getAccuracy(vector<double>& myAns, vector<double>* realAns){
    int correct_count = 0;
    for(int i = 0; i < myAns.size(); i++){
        if(myAns[i] == (*realAns)[i])
            correct_count++;
    }
    return (double)correct_count / myAns.size();
}

struct IOThreadInput{
    long tid;
    string dataset;
    vector<vector<double>>* train;
    vector<double>* categorys;
    vector<double>* colMax;
    vector<double>* colMin;
};

void* IOAndMaxMinJob(void* args){
    IOThreadInput* data = (IOThreadInput*) args;
    data->train[data->tid] = readCSV(data->dataset + TRAIN + to_string(data->tid) + CSV);
    data->categorys[data->tid] = getLastCol(data->train[data->tid]);
    data->colMax[data->tid] = getColMaxMin(data->train[data->tid], MAX);
    data->colMin[data->tid] = getColMaxMin(data->train[data->tid], MIN);
    pthread_exit((void*)data->tid);
}

vector<vector<double>> joinTrain(vector<vector<double>>* train){
    vector<vector<double>> _train;
    for(int tid = 0; tid < NTHREAD; tid++){
        for(int i = 0; i < train[tid].size(); i++){
            _train.push_back(train[tid][i]);
        }
    }
    return _train;
}

vector<double> joinCategory(vector<double>* train){
    vector<double> _train;
    for(int tid = 0; tid < NTHREAD; tid++){
        for(int i = 0; i < train[tid].size(); i++){
            _train.push_back(train[tid][i]);
        }
    }
    return _train;
}

void joinIOThreads(pthread_t* thread){
    void* status;
    for(long tid = 0; tid < NTHREAD; tid++){
		if (pthread_join(thread[tid], &status)){
			printf("ERROR; return code from pthread_create()\n");
			exit(-1);
		}
	}
}

void makeIOThreads(pthread_t* thread, vector<vector<double>>* train, IOThreadInput* args, vector<double>* categorys, string dataset, vector<double>* colMax, vector<double>* colMin){
    for(long tid = 0; tid < NTHREAD; tid++){
        args[tid].dataset = dataset;
        args[tid].train = train;
        args[tid].tid = tid;
        args[tid].categorys = categorys;
        args[tid].colMax = colMax;
        args[tid].colMin = colMin;
		if (pthread_create(&thread[tid], NULL, IOAndMaxMinJob, (void*)&args[tid])){
			printf("ERROR; return code from pthread_create()\n");
			exit(-1);
		}
	}
}

vector<double> joinMaxMin(vector<double>* col, int type){
    vector<double> result;
    for(int i = 0; i < col[0].size(); i++) result.push_back(col[0][i]);
    for(int tid = 0; tid < NTHREAD; tid++){
        for(int j = 0; j < col[tid].size(); j++){
            switch (type){
            case MIN:
                if(result[j] > col[tid][j])
                    result[j] = col[tid][j];
                break;
            case MAX:
                if(result[j] < col[tid][j])
                    result[j] = col[tid][j]; 
                break;
            } 
        }
    }
    return result;
}

struct ProcessThreadInput{
    long tid;
    vector<vector<double>>* train;
    vector<vector<double>>* weights;
    vector<double>* bias;
    vector<double>* categorys;
    vector<double>* colMax;
    vector<double>* colMin;
    double accuracy;
};

void joinProcessThreadsAndReturnAccuracy(pthread_t* thread){
    void* status;
    for(long tid = 0; tid < NTHREAD; tid++){
		if (pthread_join(thread[tid], &status)){
			printf("ERROR; return code from pthread_join()\n");
			exit(-1);
		}
	}
}


void* processJob(void* args){
    ProcessThreadInput* data = (ProcessThreadInput*)args;
    
    normalize(&(data->train[data->tid]), data->colMax, data->colMin);

    vector<vector<double>> predictions = getPrediction(data->weights, data->bias, &(data->train[data->tid]));

    vector<double> classification = getClass(predictions);

    data->accuracy = getAccuracy(classification, &(data->categorys[data->tid]));

    pthread_exit((void*)0);
}

double joinAccuracy(ProcessThreadInput* procArgs){
    double sum_accuracy = 0;
    for(int i = 0; i < NTHREAD; i++){
        sum_accuracy += procArgs[i].accuracy;
    }
    return sum_accuracy / 4;
}

int main(int argc, char** argv){
    string dataset = argv[1];
    pthread_t thread[NTHREAD];
    vector<vector<double>> train[NTHREAD];
    IOThreadInput IOArgs[NTHREAD];
    ProcessThreadInput procArgs[NTHREAD];
    vector<double> categorys[NTHREAD];
    vector<double> colMax[NTHREAD];
    vector<double> colMin[NTHREAD];

    makeIOThreads(thread, train, IOArgs, categorys, dataset, colMax, colMin);   

    vector<vector<double>> weights = readCSV(dataset + WEIGHTS);
    vector<double> bias = getLastCol(weights);

    joinIOThreads(thread);

    vector<double> total_colMax = joinMaxMin(colMax, MAX);
    vector<double> total_colMin = joinMaxMin(colMin, MIN);


    for(long tid = 0; tid < NTHREAD; tid++){
        procArgs[tid].tid = tid;
        procArgs[tid].train = train;
        procArgs[tid].weights = &weights;
        procArgs[tid].bias = &bias;
        procArgs[tid].categorys = categorys;
        procArgs[tid].colMax = &total_colMax;
        procArgs[tid].colMin = &total_colMin;
		if (pthread_create(&thread[tid], NULL, processJob, (void*)&procArgs[tid])){
			printf("ERROR; return code from pthread_create()\n");
			exit(-1);
		}
	}
    joinProcessThreadsAndReturnAccuracy(thread);
    
    printf("%.2f%%\n", joinAccuracy(procArgs) * 100);
}