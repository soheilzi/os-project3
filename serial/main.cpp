#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <time.h>

#define TRAIN "train.csv"
#define WEIGHTS "weights.csv"
#define MAX 1
#define MIN 0

using namespace std;

vector<vector<double>> readCSV(string addr){
    fstream csv;
    vector<vector<double>> out;
    vector<double> record;
    string line, word;

    csv.open(addr, ios::in);
    getline(csv, line); //flush header
    while(getline(csv, line)){
        record.clear();
        stringstream ss(line);
        while(getline(ss, word, ',')){
            record.push_back(stof(word.c_str()));
        }
        out.push_back(record);
    }
    return out;
}

vector<double> getLastCol(vector<vector<double>>& table){
    vector<double> result;
    int record_size = table[0].size();
    for(int i = 0; i < table.size(); i++){
        result.push_back(table[i][record_size - 1]);
    }
    return result;
}

vector<double> getColMaxMin(vector<vector<double>>& table, int type){
    vector<double> result;
    for(int i = 0; i < table[0].size(); i++) result.push_back(table[0][i]);
    for(int i = 0; i < table.size(); i++){
        for(int j = 0; j < table[i].size(); j++){
            switch (type){
            case MIN:
                if(result[j] > table[i][j])
                    result[j] = table[i][j];
                break;
            case MAX:
                if(result[j] < table[i][j])
                    result[j] = table[i][j]; 
                break;
            } 
        }
    }
    return result;
}

void normalize(vector<vector<double>>& table){
    vector<double> colMax = getColMaxMin(table, MAX);
    vector<double> colMin = getColMaxMin(table, MIN);
    for(int i = 0; i < table.size(); i++){
        for(int j = 0; j < table[i].size() - 1; j++){
            if(colMax[j] != colMin[j])
                table[i][j] = (table[i][j] - colMin[j]) / (colMax[j] - colMin[j]);
            else if(colMax[j] != 0)
                table[i][j] = 1;
        }
    }
}

double dotProduct(vector<double>& x, vector<double>& y){
    double result = 0;
    for(int i = 0;i < x.size() - 1; i++)
        result += x[i] * y[i];
    return result;
}

vector<vector<double>> getPrediction(vector<vector<double>>& weights, vector<double> bias, vector<vector<double>>& train){
    vector<vector<double>> predictions;
    vector<double> prediction;
    for(int i = 0; i < train.size(); i++){
        prediction.clear();
        for(int j = 0; j < weights.size(); j++){
            prediction.push_back(dotProduct(train[i], weights[j]) + bias[j]);
        }
        predictions.push_back(prediction);
    }
    return predictions;
}

double getMaxIndex(vector<double>& pred){
    int maxIndex = 0;
    double _max = pred[0];
    for(int i = 0; i < pred.size(); i++){
        if(pred[i] > _max){
            _max = pred[i];
            maxIndex = i;
        }
    } 
    return maxIndex;
}

vector<double> getClass(vector<vector<double>>& preds){
    vector<double> classes;
    for(int i = 0; i < preds.size(); i++){
        classes.push_back(getMaxIndex(preds[i]));
    }
    return classes;
}

double getAccuracy(vector<double>& myAns, vector<double>& realAns){
    int correct_count = 0;
    for(int i = 0; i < myAns.size(); i++){
        if(myAns[i] == realAns[i])
            correct_count++;
    }
    return (double)correct_count / myAns.size();
}

int main(int argc, char** argv){
    string dataset = argv[1];
    
    // clock_t tick = clock();
    vector<vector<double>> weights = readCSV(dataset + WEIGHTS);
    vector<vector<double>> train = readCSV(dataset + TRAIN);
    // printf("Reading file : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    // tick = clock();
    vector<double> bias = getLastCol(weights);
    vector<double> categorys = getLastCol(train);
    // printf("Getting bias and category : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    // tick = clock();
    normalize(train);
    // printf("Normalizing : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    // tick = clock();
    vector<vector<double>> predictions = getPrediction(weights, bias, train);
    // printf("Get predictions : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    // tick = clock();
    vector<double> classification = getClass(predictions);
    // printf("Get classes : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    // tick = clock();
    double accuracy = getAccuracy(classification, categorys);
    // printf("Get accuracy : %.3f\n", (double)(clock()-tick)/CLOCKS_PER_SEC); 

    printf("%.2f%%\n", accuracy * 100);
}